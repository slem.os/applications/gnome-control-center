#!/bin/bash

# Variables
version="3.36.2.pinephone"
# Script
tar -xzvf gnome-control-center-*mobian*.tar.gz
original_folder=$(ls gnome-control-center-*mobian*)
mv $original_folder gnome-control-center-${version}
cd gnome-control-center-${version}
for i in $(ls -d debian/patches/*.patch); do patch -p1 < $i; done
cd ..
tar -czvf gnome-control-center-${version}.tar.gz gnome-control-center-${version}
